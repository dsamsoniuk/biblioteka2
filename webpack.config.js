var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addStyleEntry('base',[
        './assets/css/app.scss'
    ])
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('app',['./assets/js/app.js'])
    .addEntry('book',['./assets/js/Book.ts'])
    .enableSassLoader()
    .enableTypeScriptLoader()
    .disableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();
