<?php

namespace App\Repository;

use App\Entity\User2book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Book;

/**
 * @method User2book|null find($id, $lockMode = null, $lockVersion = null)
 * @method User2book|null findOneBy(array $criteria, array $orderBy = null)
 * @method User2book[]    findAll()
 * @method User2book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class User2bookRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User2book::class);
    }

    public function countBorrowedBooks() {

        return $this->createQueryBuilder('u2b')
            ->innerJoin('u2b.book', 'b')
            ->addSelect('b')
            ->andWhere('u2b.book = b.id')
            ->select('COUNT(b.id) as count, b.id as book_id')
            // ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findUserBooks($user) {
        return $this->createQueryBuilder('u2b')
            ->innerJoin('u2b.book', 'b')
            ->addSelect('b')
            ->andWhere('u2b.book = b.id')
            ->andWhere('u2b.user = :id')
            ->setParameter('id',$user->getId())
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return User2book[] Returns an array of User2book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User2book
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
