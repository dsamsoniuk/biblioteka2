<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
        ->add('name', TextType::class, [
            "attr" => [
                "class" => "form-control"
            ]
        ])
        ->add('description', TextareaType::class, [
            "attr" => [
                "class" => "form-control"
            ]
        ])
        ->add('date_published', DateType::class, [
            "attr" => [
                "class" => "form-control"
            ]
        ])
        ->add('count', IntegerType::class, [
            'mapped' => false,
            "attr" => [
                "class" => "form-control"
            ]
        ])
        ->add('day_period', IntegerType::class, [
            'mapped' => false,
            "attr" => [
                "class" => "form-control"
            ]
        ])
        ->add('save', SubmitType::class, [
            'label' => 'Dodaj',
            'attr' => [
                'class' => "btn btn-primary"
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Book::class,
        ));
    }
}
