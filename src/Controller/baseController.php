<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Calculation;

class baseController  extends AbstractController {
    public function __construct(Calculation $calculation) {
        $this->calculation = $calculation;
    }
    /**
     * @Route("/", name="index_base")
     */
    public function index( Request $request) {

        // $locale = $request->getLocale();
        // dump($locale);
        // die;
        // dump($this->getUser());
        // die;
        return $this->render('main.html.twig', []);
    }
}