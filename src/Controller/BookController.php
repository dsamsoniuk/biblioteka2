<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\User2book;
use App\Entity\Book;
use App\Entity\Warehouse;

use App\Service\Calculation;

class BookController  extends AbstractController {

    public function __construct(Calculation $calculation, TranslatorInterface $translator) {
        $this->calculation = $calculation;
        $this->translator = $translator;
    }

    /**
     * @Route("/book/{page}", name="book_list")
     */
    public function books($page = 0, Request $request) {

        $limit = $this->getParameter('paginator');
 
        $available_books = $this->getDoctrine()
            ->getRepository(Warehouse::class)
            ->findAvailableBooks( $limit, $page );

        $count_books = $this->getDoctrine()
            ->getRepository(Warehouse::class)
            ->getCountBooks();

        $paginator = $this->calculation->getCountPaginator($count_books['books_count'], $limit);

        return $this->render('book/borrow.html.twig', [
            'books' => $available_books,
            'paginator' => $paginator-1
        ]);
    }

    /** 
     * @Route("/panel", name="user_panel")
     */
    public function panel() {

        $user = $this->getUser();

        $books = $this->getDoctrine()
            ->getRepository(User2book::class)
            ->findUserBooks($user);

        // dump($books);
        // die;
        return $this->render('panel/panel.html.twig', [ 
            'books' => $books
        ]);
    }

    /**
     * @Route("/borrow/{id}", name="book_borrow")
     */
    public function borrow($id) {

        $availableBook = $this->getDoctrine()
            ->getRepository(Warehouse::class)
            ->findAvailableBook($id);

        if (empty($availableBook)) { 
            $this->addFlash(
                'error',
                $this->translator->trans('Book is not available')
            );
            return new JsonResponse('fail'); // constant for 404
        }

        $availableBook->setCount( $availableBook->getCount() - 1 );

        $user = $this->getUser();
        $end_date = $this->calculation->getFutureDate( $availableBook->getDayPeriod() );

        $new_borrow = new User2book();
        $new_borrow->setUser( $user );
        $new_borrow->setBook( $availableBook->getBook() );
        $new_borrow->setDateBorrow( new \DateTIme() );
        $new_borrow->setDateBorrowEnd( $end_date );

        $em = $this->getDoctrine()->getManager();
        $em->persist($new_borrow);
        $em->persist($availableBook);
        $em->flush();

        $this->addFlash(
            'warning',
            $this->translator->trans('Book borrowed')
        );

        // Response::HTTP_NOT_FOUND
        return new JsonResponse('success'); // constant for 404
        // return new JsonResponse('no results found', Response::HTTP_NOT_FOUND); // constant for 404
    }

}