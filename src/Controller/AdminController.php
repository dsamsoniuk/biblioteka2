<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Book;
use App\Entity\Warehouse;
use App\Form\BookType;

use App\Service\Calculation;

class AdminController  extends AbstractController {

    public function __construct(Calculation $calculation, TranslatorInterface $translator) {
        $this->calculation = $calculation;
        $this->translator = $translator;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index() {

        return $this->render('admin/admin.html.twig', []);
    }

    /**
     * @Route("/admin/book/add", name="add_book")
     */
    public function add(Request $request) {

        $new_book = new Book();
        $form = $this->createForm(BookType::class, $new_book);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
           
            $count = $form->get('count')->getData();
            $dayp = $form->get('day_period')->getData();

            $ws = new Warehouse();
            $ws->setBook($new_book);
            $ws->setCount($count);
            $ws->setDayPeriod($dayp);

            $em = $this->getDoctrine()->getManager();
            $em->persist($new_book);
            $em->persist($ws);
            $em->flush();

            $this->addFlash('notice', $this->translator->trans('Book have been added'));
            return $this->redirectToRoute('add_book');

        }
        return $this->render('admin/addBook.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /** 
     * @Route("/admin/book/delete/{id}", name="book_delete")
     */
    public function delete($id) {

        $book = $this->getDoctrine()
            ->getRepository(Book::class)
            ->find($id);

        $ws = $this->getDoctrine()
            ->getRepository(Warehouse::class)
            ->findOneBy(['book' => $book]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($ws);
        $em->remove($book);
        $em->flush();
        
        $this->addFlash('notice', $this->translator->trans('Book deleted'));

        return new JsonResponse('success'); // constant for 404
    }
}