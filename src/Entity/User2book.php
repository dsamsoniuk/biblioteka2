<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User2bookRepository")
 */
class User2book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="user2books")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book")
     */
    private $book;

    /**
     * @ORM\Column(type="date")
     */
    private $date_borrow;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_give_back;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_borrow_end;


    public function __construct()
    {
        $this->date_give_back = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getDateBorrow(): ?\DateTimeInterface
    {
        return $this->date_borrow;
    }

    public function setDateBorrow(\DateTimeInterface $date_borrow): self
    {
        $this->date_borrow = $date_borrow;

        return $this;
    }

    public function getDateGiveBack(): ?\DateTimeInterface
    {
        return $this->date_give_back;
    }

    public function setDateGiveBack(\DateTimeInterface $date_give_back): self
    {
        $this->date_give_back = $date_give_back;

        return $this;
    }

    public function getDateBorrowEnd(): ?\DateTimeInterface
    {
        return $this->date_borrow_end;
    }

    public function setDateBorrowEnd(?\DateTimeInterface $date_borrow_end): self
    {
        $this->date_borrow_end = $date_borrow_end;

        return $this;
    }

}
