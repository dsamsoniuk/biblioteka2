<?php

// src/Service/MessageGenerator.php
namespace App\Service;

class Calculation
{
    public function getCountPaginator($count, $limit)
    {
        $paginator = (int)((int) $count / $limit);
        $rest = ((int) $count % $limit) > 0 ? 1 : 0;

        return $paginator + $rest;
    }

    public function getFutureDate($days) {
        $date = new \DateTime(); 
        $date = $date->modify( '+'.$days.' day' );
        return $date;
    }
}