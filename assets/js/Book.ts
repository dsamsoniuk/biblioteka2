

import { Ajaxc } from "./module/Ajaxc";

const $ = require('jquery');



(function(){

  let ajax  = new Ajaxc();

  $('.borrow-book').on('click', function(){
    let id    = parseInt( $(this).attr('bookid') );
    ajax.call('/borrow/' + id, (html: string) => {
      $(this).closest('td').html( 'ok' )
      window.location.replace("/book");
    });
  });

  $('.delete-book').on('click', function(){
    let id    = parseInt( $(this).attr('bookid') );
    ajax.call('/admin/book/delete/' + id, (html: string) => {
      $(this).closest('td').html( 'ok' )
      window.location.replace("/book");
    });
  });

})()
