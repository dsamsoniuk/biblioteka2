

class Ajaxc {

  type = "GET"

  call(path: string, callback: Function) {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        callback(this.responseText);
      }
    };

    xhttp.open(this.type, path, true);
    xhttp.send();
  }

};

export { Ajaxc };